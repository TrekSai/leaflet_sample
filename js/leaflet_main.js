
//初期表示
function llm_init() {


    // ■設定ファイル読み込み■

    // マップ中心：緯度&経度 （皇居付近） ← 設定ファイルから読み込むようにする
    var ido = 140.8825; //(139.76) //－西：＋東
    var keido = 38.26; //(35.68) //－南：＋北

    // 読み込みCSVファイル指定


    // ■CSVファイル読み込み■



    // ■Map表示■
    llm_MapOpen(ido, keido);

}


//leaflet OSM map
function llm_MapOpen(ido, keido) {

    // マップスケール
    var scale = 14;

    //Map Objet作成
    var map = L.map('map_elemnt');
    //マップ中心：経度＆緯度、倍率設定
    map.setView([keido, ido], scale);

    // tile layerをOpenStreetMapに設定
    var tileLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution : '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                });
                tileLayer.addTo(map);

    // マーカー設定(不要)
    var mapMarker = L.marker([keido, ido]);
    mapMarker.addTo(map);
    //mapMarker.bindPopup('仙台駅');
    mapMarker.openPopup();

    // ヒートマップ設定




    // ベースレイヤーの設定
    var baseLayers = {
        "OpenStreetMap": tileLayer
    };

    // マーカーレイヤーの設定
    var overlays = {
        "Marker": mapMarker,
    };

    // ヒートマップレイヤーの設定




    // レイヤーの設定
    L.control.layers(baseLayers, overlays).addTo(map);

    // add control scale
    L.control.scale().addTo(map);
}