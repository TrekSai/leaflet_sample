/* 20150318 */
function llm_init() {
	// ■設定ファイル読み込み■
	// マップ中心セット： 設定ファイルから読み込むようにする（事業所とかに変更？）
	var setlat = 38.26886; //－南：＋北
	var setlng = 140.872353; //－西：＋東
	var setscale = 12;
	// ■Map表示■
	llm_MapOpen(setlat, setlng, setscale);

}
function llm_MapOpen(setlat, setlng, setscale) {
	var map = L.map('map').setView([setlat, setlng], setscale);
	// tileLayer set
	var osmTileLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
		attribution : '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
		minZoom: 7
	}).addTo(map); //はじめにOSMを表示
	var gsiTileLayer = L.tileLayer('http://cyberjapandata.gsi.go.jp/xyz/std/{z}/{x}/{y}.png', {
		attribution: "<a href='http://portal.cyberjapan.jp/help/termsofuse.html' target='_blank'>国土地理院</a>",
		minZoom: 7
	});
	//point_data.jsから地区のpointData取得
	var heat = L.heatLayer(districtPointData, {
		radius: 20,	// 半径設定 default:20
		blur: 50,	// 低いほど赤い?
		maxZoom: 17,
	}).addTo(map);

	// welfare 福祉施設レイヤ
	var welfare = L.marker([38.2905358, 140.8987657]).addTo(map);


	// var welfareLayer = L.marker(welfarePointData).addTo(map);
	welfare.bindPopup("住所or施設名");

	// 事業所拠点のマーカーにicon設定
	var setIcon =  L.icon({
		iconUrl: '../data/icon_home.png',
	    iconSize: [40, 40]
	});
	var homeMarkers = L.marker([setlat, setlng], {icon: setIcon}).addTo(map);
	homeMarkers.bindPopup("homebase");

	// add layers
	var baseLayers = {
			"GSI": gsiTileLayer,
			"OpenStreetMap": osmTileLayer
	};
	var overLayers = {
			"Heatmap": heat,
			"Welfaremap": welfare
	};

	L.control.layers(baseLayers, overLayers).addTo(map);
	// 縮尺コントロール追加
	L.control.scale().addTo(map);

	// 凡例
	var legend = L.control({position: 'bottomright'});

	legend.onAdd = function (map) {

	    var div = L.DomUtil.create('div', 'info legend'),
	        grades = [0, 50, 100, 200, 300],
	        labels = ['<strong>凡例</strong>'];

	    // loop through our density intervals and generate a label with a colored square for each interval
	    for (var i = 0; i < grades.length; i++) {
	        div.innerHTML +=
	            '<i style="background:' + getColor(grades[i] + 1) + '"></i> ' +
	            grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '<br>' : '+');
	    }

	    return div;
	};
	legend.addTo(map);
}

function getColor(d) {
    return d > 300  ? 'red' :
           d > 200  ? 'orange' :
           d > 100  ? 'yellow' :
           d > 50   ? 'lime' :
           d > 0   ? 'cyan' :
                      '#FFEDA0';
}
// ヒートマップ作成
// canvasから離れた場合の処理を加える
/*
(c) 2014, Vladimir Agafonkin
simpleheat, a tiny JavaScript library for drawing heatmaps with Canvas
https://github.com/mourner/simpleheat
*/
! function() {
	"use strict";
	function t(i) {
		return this instanceof t ?
				(this._canvas = i = "string" == typeof i ? document.getElementById(i) : i,
				this._ctx = i.getContext("2d"),
				this._width = i.width,
				this._height = i.height,
				this._max = 1, void this.clear()) : new t(i)
	}
	t.prototype = {
			defaultRadius: 30,
			defaultGradient: {
				.2: "cyan",
				.4: "lime",
				.6: "yellow",
				.8: "orange",
				1: "red"
			},
			lowGradient: {
				.3: "lightgreen",
				.6: "lime",
				.8: "yellow"
			},
			highGradient: {
				.1: "lightgreen",
				.2: "lime",
				.4: "yellow",
				.6: "orange",
				.8: "red"
			},
			data: function(t) {
				return this._data = t, this
			},
			max: function(t) {
				return this._max = t, this
			},
			add: function(t) {
				return this._data.push(t), this
			},
			clear: function() {
				return this._data = [], this
			},
			radius: function(radius, blur) {
				// (t, i) heatの(radius, blur)
				blur = blur || 15;

				var a = this._circle = document.createElement("canvas");
				var s = a.getContext("2d");
				var e = this._r = radius + blur;
				// arc(x, y, radius, startAngle, endAngle, anticlockwise)
				return a.width = a.height = 2 * e,
					s.shadowOffsetX = s.shadowOffsetY = 200,
					s.shadowBlur = blur,
					s.shadowColor = "black",
					s.beginPath(),
					s.arc(e - 200, e - 200, radius, 0, 2 * Math.PI, !0),
					s.closePath(),
					s.fill(),
					this
			},
			gradient: function(t) {
				//gradient
				var i = document.createElement("canvas"),
				a = i.getContext("2d"),
				s = a.createLinearGradient(0, 0, 0, 256);
				i.width = 1,
				i.height = 256;
				for (var e in t) {
					s.addColorStop(e, t[e]);
				}
				return a.fillStyle = s, a.fillRect(0, 0, 1, 256), this._grad = a.getImageData(0, 0, 1, 256).data, this
			},
			draw: function(t) {
				this._circle || this.radius(this.defaultRadius);
				// console.log(this.defaultGradient);
				this._grad || this.gradient(this.defaultGradient);

				var i = this._ctx;
				i.clearRect(0, 0, this._width, this._height);
				for (var a, s = 0; s < this._data.length; s++) {
					// console.log("153",this._data[s]);
					a = this._data[s],
					i.globalAlpha = Math.max(a[2] / this._max, t || .05), //引数として与えた複数の数の中で最大の数を返します。
					i.drawImage(this._circle, a[0] - this._r, a[1] - this._r);
				}
				var n = i.getImageData(0, 0, this._width, this._height);
				return this._colorize(n.data, this._grad), i.putImageData(n, 0, 0), this
			},
			_colorize: function(t, i) {
				for (var a, s = 3; s < t.length; s += 4) {
					a = 4 * t[s],
					a && (t[s - 3] = i[a], t[s - 2] = i[a + 1], t[s - 1] = i[a + 2])
				}
			}
	}, window.simpleheat = t
}(),
/*
(c) 2014, Vladimir Agafonkin
Leaflet.heat, a tiny and fast heatmap plugin for Leaflet.
https://github.com/Leaflet/Leaflet.heat
*/
L.HeatLayer = (L.Layer ? L.Layer : L.Class).extend({
	options: {
        minOpacity: 0.05,
        maxZoom: 18,
        radius: 20,
        blur: 50,
        max: 1.0
   },
   initialize: function (latlngs, options) {
       this._latlngs = latlngs;
       L.setOptions(this, options);
   },
   setLatLngs: function (latlngs) {
       this._latlngs = latlngs; //no
       return this.redraw();
   },
   addLatLng: function (latlng) {
       this._latlngs.push(latlng); //no
       return this.redraw();
   },
   setOptions: function (options) {
       L.setOptions(this, options); //no
       if (this._heat) {
           this._updateOptions();
       }
       return this.redraw();
   },
   redraw: function () {
       if (this._heat && !this._frame && !this._map._animating) {
           this._frame = L.Util.requestAnimFrame(this._redraw, this);
       }
       return this;
   },
   onAdd: function (map) {
       this._map = map;
       // this._canvas = undefined
       if (!this._canvas) {
           this._initCanvas();
       }
       map._panes.overlayPane.appendChild(this._canvas);
       map.on('moveend', this._reset, this);
       if (map.options.zoomAnimation && L.Browser.any3d) {
           map.on('zoomanim', this._animateZoom, this);
       }
       this._reset();
   },
   onRemove: function (map) {
       map.getPanes().overlayPane.removeChild(this._canvas);
       map.off('moveend', this._reset, this);
       if (map.options.zoomAnimation) {
           map.off('zoomanim', this._animateZoom, this);
       }
   },
   addTo: function (map) {
       map.addLayer(this);
       return this;
   },
   _initCanvas: function () {
       var canvas = this._canvas = L.DomUtil.create('canvas', 'leaflet-heatmap-layer leaflet-layer');
       var size = this._map.getSize();
       canvas.width  = size.x;
       canvas.height = size.y;
       var animated = this._map.options.zoomAnimation && L.Browser.any3d;
       L.DomUtil.addClass(canvas, 'leaflet-zoom-' + (animated ? 'animated' : 'hide'));
       this._heat = simpleheat(canvas);
       this._updateOptions();
   },
   _updateOptions: function () {
       this._heat.radius(this.options.radius || this._heat.defaultRadius, this.options.blur);
       // this.options.gradient undefined
       if (this.options.gradient) {
           this._heat.gradient(this.options.gradient);
       }
       // this.options.max undefined
       if (this.options.max) {
           this._heat.max(this.options.max);
       }
   },
   _reset: function () {
       var topLeft = this._map.containerPointToLayerPoint([0, 0]);
       L.DomUtil.setPosition(this._canvas, topLeft);
       var size = this._map.getSize();
       if (this._heat._width !== size.x) {
           this._canvas.width = this._heat._width  = size.x;
       }
       if (this._heat._height !== size.y) {
           this._canvas.height = this._heat._height = size.y;
       }
       this._redraw();
   },
   _redraw: function () {
       var data = [],
           r = this._heat._r,
           size = this._map.getSize(),
           bounds = new L.LatLngBounds(
               this._map.containerPointToLatLng(L.point([-r, -r])),
               this._map.containerPointToLatLng(size.add([r, r]))
               ),
           maxZoom = this.options.maxZoom === undefined ? this._map.getMaxZoom() : this.options.maxZoom,
           v = 1 / Math.pow(2, Math.max(0, Math.min(maxZoom - this._map.getZoom(), 12))),
           cellSize = r / 2,
           grid = [],
           panePos = this._map._getMapPanePos(),
           offsetX = panePos.x % cellSize,
           offsetY = panePos.y % cellSize,
           i, len, p, cell, x, y, j, len2, k;


       for (i = 0, len = this._latlngs.length; i < len; i++) {
           if (bounds.contains(this._latlngs[i])) {
               p = this._map.latLngToContainerPoint(this._latlngs[i]);
               x = Math.floor((p.x - offsetX) / cellSize) + 2;
               y = Math.floor((p.y - offsetY) / cellSize) + 2;
               // 高齢者人口 console.log(this._latlngs[i][2]);
               var alt =
                   this._latlngs[i].alt !== undefined ? this._latlngs[i].alt :
                   this._latlngs[i][2] !== undefined ? +this._latlngs[i][2] : 1;
               k = alt * v;
               grid[y] = grid[y] || [];
               cell = grid[y][x];
               if (!cell) {
            	   grid[y][x] = [p.x, p.y, k];
               } else {
                   cell[0] = (cell[0] * cell[2] + p.x * k) / (cell[2] + k); // x
                   cell[1] = (cell[1] * cell[2] + p.y * k) / (cell[2] + k); // y
                   cell[2] += k; // cumulated intensity value
               }
           }
       }
       for (i = 0, len = grid.length; i < len; i++) {
           if (grid[i]) {
               for (j = 0, len2 = grid[i].length; j < len2; j++) {
                   cell = grid[i][j];
                   if (cell) {
                       data.push([
                           Math.round(cell[0]),
                           Math.round(cell[1]),
                           Math.min(cell[2], 1)
                       ]);
                   }
               }
           }
       }
       this._heat.data(data).draw(this.options.minOpacity);
       this._frame = null;
   },
   _animateZoom: function (e) {
       var scale = this._map.getZoomScale(e.zoom),
           offset = this._map._getCenterOffset(e.center)._multiplyBy(-scale).subtract(this._map._getMapPanePos());

       if (L.DomUtil.setTransform) {
          L.DomUtil.setTransform(this._canvas, offset, scale);

       } else {
           this._canvas.style[L.DomUtil.TRANSFORM] = L.DomUtil.getTranslateString(offset) + ' scale(' + scale + ')';
       }
   }
});
L.heatLayer = function (latlngs, options) {
   return new L.HeatLayer(latlngs, options);
};