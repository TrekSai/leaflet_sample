// wget
// var f = require('./nodetest.js');

//必要なデータを整形
//データをまとめて、heatmapに反映させる

//■CSVファイル読み込み■
function getCsvFile(csvFilePath) { //csvﾌｧｲﾙﾉ相対ﾊﾟｽor絶対ﾊﾟｽ
	var csvData = new Array();
	var data = new XMLHttpRequest();
	data.open("GET", csvFilePath, false); //true:非同期,false:同期
	data.send(null);

	var lines = data.responseText.split("\n");

	// lines.length だとデータ多い？
	for (var i = 1; i < lines.length; i++) {
		var cells = lines[i].split(",");
		if( cells.length != 1 ) {
			// 地区名=0,総人口=1,65歳以上人口=2,65歳以上比率=3,緯度=4,経度=5
			csvData.push([ +cells[4], +cells[5], +cells[2] ]);
		}
	}
	return csvData;
}
var csvFilePath = "../data/eldery.ratio.with.headers.csv";
var pointData = getCsvFile(csvFilePath);